<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "competition".
 *
 * @property integer $competition_id
 * @property string $name
 * @property string $description
 * @property string $date_of_create
 */
class Competition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'competition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['date_of_create'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'competition_id' => 'Competition ID',
            'name' => 'Name',
            'description' => 'Description',
            'date_of_create' => 'Date Of Create',
        ];
    }

    public static function getCountCompetition()
    {
        return self::find()
            ->count();
    }

    public static function getNameCompetition($competition_id)
    {
        $nameCompetition = self::find()
            ->asArray()
            ->select(['name'])
            ->where(['competition_id' => $competition_id])
            ->one();
        return array_shift($nameCompetition);
    }

    public static function getAllCompetition()
    {
        return self::find()
            ->asArray()
            ->select('*')
            ->orderBy(['competition_id' => SORT_DESC])
            ->all();
    }
}
