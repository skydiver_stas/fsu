<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "competition_documents".
 *
 * @property integer $document_id
 * @property string $name
 * @property string $description
 * @property string $name_fille
 * @property integer $competition_id
 * @property string $date_of_create
 */
class CompetitionDocuments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'competition_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'name_fille', 'competition_id'], 'required'],
            [['description'], 'string'],
            [['competition_id'], 'integer'],
            [['date_of_create'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name_fille'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'name' => 'Name',
            'description' => 'Description',
            'name_fille' => 'Name Fille',
            'competition_id' => 'Competition ID',
            'date_of_create' => 'Date Of Create',
        ];
    }

    public static function getAllDocuments($competition_id)
    {
        return self::find()
            ->asArray()
            ->select('*')
            ->where(['competition_id' => $competition_id])
            ->all();
    }
}
