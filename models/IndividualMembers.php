<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "individual_members".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $category
 * @property integer $status
 */
class IndividualMembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'individual_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'category', 'status'], 'required'],
            [['status'], 'integer'],
            [['full_name'], 'string', 'max' => 255],
            [['category'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'ПІпБ',
            'category' => 'Сертіфікат',
            'status' => 'Статус',
        ];
    }

    public static function getCount()
    {
        return self::find()
            ->count();
    }

    public static function getAllIndividualMembers()
    {
        return self::find()
           // ->asArray()
            ->select(['full_name', 'category', 'status'])
            ->orderBy(['id' => SORT_DESC]);
           // ->all();
    }


}
