<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "carousel".
 *
 * @property integer $id
 * @property string $title
 * @property string $file_name
 * @property integer $active
 */
class Carousel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carousel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'file_name', 'active'], 'required'],
            [['active'], 'integer'],
            [['title', 'file_name'], 'string', 'max' => 125]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'file_name' => 'File Name',
            'active' => 'Active',
        ];
    }

    public static function getImageCarousel()
    {
        return self::find()
            ->asArray()
            ->select(['file_name'])
            ->where(['active' => 1])
            ->all();
    }
}
