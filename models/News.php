<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $deskription
 * @property integer $category_news
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'deskription', 'category_news'], 'required'],
            [['deskription'], 'string'],
            [['category_news'], 'integer'],
            [['title'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'deskription' => 'Deskription',
            'category_news' => 'Category News',
        ];
    }

    public static function getCount($year)
    {
        return News::find()
        ->where("YEAR(date) = $year")
        ->count();
    }

    public static function getAllNews($offset, $limit)
    {   $date = YEAR_NOW;

        return News::find()
            ->select(['id', 'title', 'deskription', 'date', 'image'])
            ->where("YEAR(date) = $date")
            ->orderBy(['id' => SORT_DESC])
            ->offset($offset)
            ->limit($limit)
            ->asArray()
            ->all();

    }

    public static function getNews($id)
    {
        return self::find()
            ->asArray()
            ->select(['*', 'YEAR(date) AS year'])
            ->where(['id' => $id])
            ->one();

    }

    public static function getArchiveNews($year, $offset, $limit)
    {
        return self::find()
            ->select(['id', 'title', 'deskription', 'date', 'image'])
            ->where("YEAR(date) = $year")
            ->orderBy(['id' => SORT_DESC])
            ->offset($offset)
            ->limit($limit)
            ->asArray()
            ->all();

    }
}
