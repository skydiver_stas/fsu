<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documents_pdf".
 *
 * @property integer $document_id
 * @property string $name
 * @property string $description
 * @property string $name_file
 * @property integer $category_id
 * @property string $date_of_creation
 */
class Off extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents_pdf';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'name_file', 'category_id'], 'required'],
            [['description'], 'string'],
            [['category_id'], 'integer'],
            [['date_of_creation'], 'safe'],
            [['name'], 'string', 'max' => 25],
            [['name_file'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'name' => 'Name',
            'description' => 'Description',
            'name_file' => 'Name File',
            'category_id' => 'Category ID',
            'date_of_creation' => 'Date Of Creation',
        ];
    }

    public static function getCount($category_id)
    {
        return self::find()
            ->where(['category_id' => $category_id])
            ->count();
    }

    public static function getAllDocuments($category_id ,$offset, $limit)
    {
        return self::find()
            ->select(['document_id', 'name', 'description', 'name_file', 'category_id', 'date_of_creation'])
            ->where(['category_id' => $category_id])
            ->orderBy(['document_id' => SORT_DESC])
            ->offset($offset)
            ->limit($limit)
            ->all();

    }


}
