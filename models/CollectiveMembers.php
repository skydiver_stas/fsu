<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "collective_members".
 *
 * @property integer $id
 * @property string $name
 * @property string $region
 * @property string $contacts
 */
class CollectiveMembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collective_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'region', 'contacts'], 'required'],
            [['region', 'contacts'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'region' => 'Region',
            'contacts' => 'Contacts',
        ];
    }

    public static function getAllMembers()
    {
        return self::find()
            ->asArray()
            ->select(['*'])
            ->all();
    }
}
