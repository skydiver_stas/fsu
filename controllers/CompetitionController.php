<?php

namespace app\controllers;

use app\models\Competition;
use yii\data\Pagination;
use app\models\CompetitionDocuments;

class CompetitionController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $listCompetition = Competition::getAllCompetition();
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => Competition::getCountCompetition(),
        ]);

        return $this->render('index',[
            'listCompetition' => $listCompetition,
            'pagination' => $pagination,
        ]);
    }

    public function actionCompetitionDocuments($competition_id)
    {
        $listDocuments = CompetitionDocuments::getAllDocuments($competition_id);
        $nameCompetition = Competition::getNameCompetition($competition_id);

        return $this->render('competition-documents',[
            'listDocuments' => $listDocuments,
            'nameCompetition' => $nameCompetition,
        ]);
    }

}
