<?php

namespace app\controllers;

use app\models\Off;
use yii\data\Pagination;
use yii\helpers\VarDumper;

class OffController extends \yii\web\Controller
{
    public function actionIndex($category_id)
    {
        $count_documents = Off::getCount($category_id);

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $count_documents,
        ]);

        $allDocuments = Off::getAllDocuments($category_id, $pagination->offset, $pagination->limit);
        $categoryName = (new \yii\db\Query())
            ->select(['name'])
            ->from('category_documents')
            ->where(['category_id' => $category_id])
            ->one();

        return $this->render('index', [
            'allDocuments' => $allDocuments,
            'categoryName' => $categoryName,
            'pagination' => $pagination,
            'category_id' => $category_id,
        ]);
    }

}
