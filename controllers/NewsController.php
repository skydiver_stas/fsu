<?php

namespace app\controllers;

use Yii;
use app\models\News;
use yii\data\Pagination;



class NewsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $year = date("Y");
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => News::getCount($year),
        ]);

        $allNews = News::getAllNews($pagination->offset, $pagination->limit);

        return $this->render('index', [
            'allNews' => $allNews,
            'pagination' => $pagination,
        ]);
    }


    public static function getImage($image)
    {
        if(!empty($image))
        {
            $path_image = PATH_IMAGES_SHOW.$image;
        }

        else
        {
            $path_image = PATH_IMAGE_SHOW.'no_image.png';
        }

        return $path_image;
    }

    public function actionViewNews($id)
    {

        $news = News::getNews($id);
        return $this->render('news', ['news' => $news]);

    }

    public function actionArchive()
    {
        $today = YEAR_NOW;
        $dateCreateSite = DATE_CREATE_SITE;
        $archiveYears = [];



        if($today != $dateCreateSite){

            while($today != $dateCreateSite)
            {
                $archiveYears[] = $dateCreateSite++;

            }
        } else {
            $archiveYears = 'Архів поки що порожній';
        }

        return $this->render('archive', [
            'archiveYears' => $archiveYears,
            'pagination' => '',
        ]);
    }

    public function actionArchiveNews($year)
    {
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => News::getCount($year),
        ]);

        $allNews = News::getArchiveNews($year, $pagination->offset, $pagination->limit);

        return $this->render('archiveAllNews', [
            'allNews' => $allNews,
            'pagination' => $pagination,
            'year' => $year,
        ]);
    }

    public function actionViewArchiveNews($id)
    {
        $news = News::getNews($id);
        return $this->render('newsArchive', ['news' => $news]);
    }
}
