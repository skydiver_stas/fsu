<?php

namespace app\controllers;

use app\models\CollectiveMembers;

class CollectiveMembersController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $listMembers = CollectiveMembers::getAllMembers();

        return $this->render('index',
            [
                'listMembers' => $listMembers,
            ]);
    }

}
