<?php

namespace app\controllers;

use Yii;
use app\models\IndividualMembers;
use app\models\IndividualMembersSearch;

class MembersController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionIndividual()
    {
        $searchModel = new IndividualMembersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('individual',[
            'searchModel' => $searchModel,
            'listDataProvider' => $dataProvider,

        ]);
    }

}
