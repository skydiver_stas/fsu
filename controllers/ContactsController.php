<?php

namespace app\controllers;

use app\models\Contacts;

class ContactsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $contactsList = Contacts::getContacts();

        return $this->render('index',
        [
            'contactsList' => $contactsList,
        ]);
    }

}
