<?php


use yii\bootstrap\Carousel;
/* @var $this yii\web\View */

$this->title = 'Головна';

$images = [];
foreach(app\models\Carousel::getImageCarousel() as $key)
{
    $images[] = '<img class="image-carousel" src="'.PATH_CAROUSEL.$key['file_name'].'"/>';
}

?>

<?php

echo Carousel::widget ( [
    'items' => $images
]);

?>


