<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Контакти';
$this->params['breadcrumbs'][] = $this->title;


?>
<h1><?= $this->title ?></h1>
<?php foreach($contactsList as $key => $value): ?>
    <div class="contacts">

        <?= nl2br($value); ?>

    </div>
<?php endforeach; ?>
