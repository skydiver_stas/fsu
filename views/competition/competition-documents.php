<?php
/* @var $this yii\web\View */
use yii\helpers\Html;


$this->title = $nameCompetition;
$this->params['breadcrumbs'][] = ['label' => 'Змагання', 'url' => ['competition/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= Html::encode($this->title) ?></h1>

<p>
<?php if(!empty($listDocuments)): ?>
    <?php foreach($listDocuments as $key): ?>
    <div class="documents">
        <?php

        $name_file;
        if(!empty($key['name_fille'])){
            $name_file = PATH_DOCUMENT_FOR_COMPETITION_SHOW.$key['name_fille'];
            $class = 'active';
            $alert = 'button-active';
        } else {
            $name_file = '#';
            $class = 'disabled';
            $alert = 'button-disabled';;
        }
        ?>
        <div class="text-documents">
            <h3><?= Html::encode($key['name']); ?></h3>
            <p><?= nl2br($key['description']); ?></p>
        </div>
        <div class="button-documents">
            <button class="<?= $alert ?>"><a href="<?= $name_file ?>" class="<?= $class ?>" target="_blank" >Показати</a></button>
            <button class="<?= $alert ?>"><a href="<?= $name_file ?>" class="<?= $class ?>" download>Завантажити</a></button>
        </div>
    </div>
    <div class="documents-date">
        <p>Дата створення - <?= $key['date_of_create']; ?></p>
    </div>
<?php endforeach; ?>
<?php endif; ?>

</p>
