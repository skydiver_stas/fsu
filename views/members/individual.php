<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Індивідуальні члени ФПСУ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calendar-competition-index">

<h1><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
    'dataProvider' => $listDataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'full_name',
        'category',
        'status',
        ],

    ]); ?>
</div>


