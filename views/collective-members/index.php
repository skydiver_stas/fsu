<?php

use yii\helpers\Html;

$this->title = 'Колективні члени ФПСУ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calendar-competition-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach($listMembers as $key): ?>
    <div class="collective-members">

       <div class="name-collective-members"><h4><?= nl2br($key['name']); ?></h4></div>
       <div class="left-collective-members"><?= nl2br($key['region']); ?></div>
       <div class="right-collective-members"><?= nl2br($key['contacts']); ?></div>

    </div>
    <?php endforeach; ?>
</div>
