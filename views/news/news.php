<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Новина';
$this->params['breadcrumbs'][] = ['label' => 'Новини', 'url' => ['index']];
$this->params['breadcrumbs'][] = $news['title'];
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="news">
    <h3><?= Html::encode($news['title']); ?></h3>
    <p><?= nl2br($news['full_deskription']); ?></p>
</div>

<div class="news-date">
    <p>Дата публікації - <?= $news['date']; ?></p>
</div>


