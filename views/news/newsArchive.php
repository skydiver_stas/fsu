<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Новина з архіву';
$this->params['breadcrumbs'][] = ['label' => 'Архів', 'url' => ['archive']];
$this->params['breadcrumbs'][] = ['label' => 'Новини за '.$news['year'], 'url' => ['news/archive-news', 'year' => $news['year']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="news">
        <h3><?= Html::encode($news['title']); ?></h3>
        <p><?= nl2br($news['full_deskription']); ?></p>
        <p><?= $news['date']; ?></p>
</div>
