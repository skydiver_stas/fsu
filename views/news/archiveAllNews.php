<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\controllers\NewsController;



$this->title = 'Новини за '.$year;
$this->params['breadcrumbs'][] = ['label' => 'Архів', 'url' => ['archive']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<?php if(!empty($allNews)): ?>
<?php foreach($allNews as $key): ?>
    <div class="news">
        <div class="text-news">
            <h3><?= Html::encode($key['title']); ?></h3>
            <p><?= nl2br($key['deskription']); ?></p>
            <p><a href="<?= Yii::$app->urlManager->createUrl(['news/view-archive-news', 'id' => $key['id']]); ?>">Чітати повністю</a></p>
        </div>

        <div class="image-news" >
            <img src="<?= NewsController::getImage($key['image']); ?>">
        </div>
    </div>
    <div class="news-date">
        <p>Дата публікації - <?= $key['date']; ?></p>
    </div>
<?php endforeach; ?>
<?= LinkPager::widget(['pagination' => $pagination]) ?>
<?php endif; ?>

