<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 29.03.16
 * Time: 19:46
 */

$this->title = 'Архів';
$this->params['breadcrumbs'][] = $this->title;

?>



<?php if(is_array($archiveYears)): ?>
    <h3>Архів новин по рокам</h3>
    <?php foreach($archiveYears as $key => $value): ?>
        <button><a href="<?= Yii::$app->urlManager->createUrl(['competition/archive-competition', 'year' => $value]); ?>"><?= $value ?></a></button>
    <?php endforeach; ?>
<?php endif; ?>

<?php if(!is_array($archiveYears)): ?>
    <h3><?= $archiveYears ?></h3>
<?php endif; ?>