<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = $competition['name'];
$this->params['breadcrumbs'][] = ['label' => 'Змагання', 'url' => ['competition/index/']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="news">
    <h3><?= Html::encode($competition['name']); ?></h3>
    <p><?= nl2br($competition['description']); ?></p>

    <p>
        <?php

        if(!empty($competition['regulation'])){
            $regulation = PATH_DOCUMENT_SHOW.$competition['regulation'];
            $classRegulation = 'active';
            $alertPegulation = 'button-active';
        } else {
            $regulation = '#';
            $classRegulation = 'disabled';
            $alertPegulation = 'button-disabled';
        }

        if(!empty($competition['provisions'])){
            $provisions = PATH_DOCUMENT_SHOW.$competition['provisions'];
            $classProvisions = 'active';
            $alertProvisions = 'button-active';
        } else {
            $provisions = '#';
            $classProvisions = 'disabled';
            $alertProvisions = 'button-disabled';
        }

        if(!empty($competition['protocols'])){
            $protocols = PATH_DOCUMENT_SHOW.$competition['protocols'];
            $classProtocols = 'active';
            $alertProtocols = 'button-active';
        } else {
            $protocols = '#';
            $classProtocols = 'disabled';
            $alertProtocols = 'button-disabled';
        }
        ?>
        <h4>Правила</h4>
        <button class="<?= $alertPegulation ?>"><a href="<?= $regulation; ?>" class="<?= $classRegulation ?>" target="_blank" >Показати</a></button>
        <button class="<?= $alertPegulation ?>"><a href="<?= $regulation; ?>" class="<?= $classRegulation ?>" download>Завантажити</a></button>
    </p>

    <p>
        <h4>Положення</h4>
        <button class="<?= $alertProvisions ?>"><a href="<?= $provisions; ?>" class="<?= $classProvisions ?>" target="_blank" >Показати</a></button>
        <button class="<?= $alertProvisions ?>"><a href="<?= $provisions; ?>" class="<?= $classProvisions ?>" download>Завантажити</a></button>
    </p>

    <p>
    <h4>Протоколи</h4>
    <button class="<?= $alertProtocols ?>"><a href="<?= $protocols; ?>" class="<?= $classProtocols ?>" target="_blank" >Показати</a></button>
    <button class="<?= $alertProtocols ?>"><a href="<?= $protocols; ?>" class="<?= $classProtocols ?>" download>Завантажити</a></button>
    </p>

</div>
<div class="news-date">
    <p>Дата публікації - <?= $competition['date']; ?></p>
</div>