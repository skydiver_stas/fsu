<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Змагання';
$this->params['breadcrumbs'][] = $this->title;


?>
<h1><?= $this->title ?></h1>

<p>
    <?php foreach($allCompetition as $key): ?>
    <div class="documents">
        <div class="text-documents">
            <h3><?= Html::encode($key['name']); ?></h3>
        </div>
        <div class="button-documents">
            <button><a href="<?= Yii::$app->urlManager->createUrl(['competition/view-competition', 'id' => $key['id']]); ?>" cursor="default" >Переглянути деталі</a></button>
        </div>
    </div>

    <div class="documents-date">
        <p>Дата створення - <?= $key['date']; ?></p>
    </div>
    <?php endforeach; ?>
</p>
<?= LinkPager::widget(['pagination' => $pagination]); ?>

