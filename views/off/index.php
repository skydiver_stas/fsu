<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = $categoryName['name'];

$this->params['breadcrumbs'][] = $categoryName['name'];
?>
<h1><?= Html::encode($this->title) ?></h1>

<p>

    <?php foreach($allDocuments as $key): ?>
    <div class="documents">
            <?php

                $name_file;
                if(!empty($key['name_file'])){
                    $name_file = PATH_DOCUMENT_SHOW.$key['name_file'];
                    $class = 'active';
                    $alert = 'button-active';
                } else {
                    $name_file = '#';
                    $class = 'disabled';
                    $alert = 'button-disabled';;
                }
            ?>
    <div class="text-documents">
        <h3><?= Html::encode($key['name']); ?></h3>
        <p><?= nl2br($key['description']); ?></p>
    </div>
    <div class="button-documents">
        <button class="<?= $alert ?>"><a href="<?= $name_file ?>" class="<?= $class ?>" target="_blank" >Показати</a></button>
        <button class="<?= $alert ?>"><a href="<?= $name_file ?>" class="<?= $class ?>" download>Завантажити</a></button>
    </div>
    </div>
    <div class="documents-date">
        <p>Дата створення - <?= $key['date_of_creation']; ?></p>
    </div>
    <?php endforeach; ?>
<?= LinkPager::widget(['pagination' => $pagination]) ?>

</p>
