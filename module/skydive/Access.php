<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 07.04.16
 * Time: 12:12
 */

namespace app\module\skydive;

use Yii;
use yii\web\Controller;

class Access extends Controller{


    public function beforeAction($action)
    {

        if (!parent::beforeAction($action))
        {
            return false;
        }

        if (!Yii::$app->user->isGuest)
        {
            return true;
        }
        else
        {
            Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
            //для перестраховки вернем false
            return false;
        }
    }
} 