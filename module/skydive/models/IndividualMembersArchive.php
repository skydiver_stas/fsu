<?php

namespace app\module\skydive\models;

use Yii;

/**

 * This is the model class for table "individual_members_archive".

 *
 * @property integer $id
 * @property string $full_name
 * @property string $category
 * @property string $status

 */
class IndividualMembersArchive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'individual_members_archive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'category', 'status'], 'required'],
            [['full_name'], 'string', 'max' => 255],
            [['category', 'status'], 'string', 'max' => 16]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'ПІпБ',
            'category' => 'Категорія',
            'status' => 'Статус',
        ];
    }


    public static function getAllMembers()
    {
        return self::find()
            ->asArray()
            ->select(['*'])
            ->all();

    }
}
