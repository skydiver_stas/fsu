<?php

namespace app\module\skydive\models;

use Yii;

/**
 * This is the model class for table "competition".
 *
 * @property integer $competition_id
 * @property string $name
 * @property string $description
 * @property string $date_of_create
 */
class Competition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'competition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['date_of_create'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'competition_id' => 'Competition ID',
            'name' => 'Назва змагання',
            'description' => 'Повний опис',
            'date_of_create' => 'Дата створення',
        ];
    }

    public static function getIdCompetition()
    {
        $documents_list = self::find()
            ->asArray()
            ->select(['competition_id', 'name'])
            ->all();

        $list = [];
        foreach($documents_list as $key)
        {
            $list += [$key['competition_id'] => $key['name']];
        }

        return $list;
    }

    public static function getNameCompetition($competition_id)
    {
        $nameCompetition = self::find()
            ->asArray()
            ->select('name')
            ->one();

        return array_shift($nameCompetition);
    }

    public static function getCountCompetition()
    {
        return self::find()
            ->count();
    }

    public static function getAllCompetition($offset, $limit)
    {
       return self::find()
            ->asArray()
            ->select('*')
            ->orderBy(['competition_id' => SORT_DESC])
            ->offset($offset)
            ->limit($limit)
            ->all();
    }
}
