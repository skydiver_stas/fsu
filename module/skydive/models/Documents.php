<?php

namespace app\module\skydive\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\Query;





/**
 * This is the model class for table "documents_pdf".
 *
 * @property integer $document_id
 * @property string $name
 * @property string $description
 * @property string $name_file
 * @property integer $category_id
 * @property string $date_of_creation
 */
class Documents extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents_pdf';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'category_id'], 'required'],
            [['description'], 'string'],
            [['category_id'], 'integer'],
            [['date_of_creation'], 'safe'],
            [['name'], 'string', 'max' => 25],
            [['name_file'], 'string', 'max' => 120],
            [['file'], 'file', 'extensions' => 'pdf']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'name' => 'Імя Файлу',
            'description' => 'Короткий опис',
            'name_file' => 'Name File',
            'file' => 'Файл',
            'category_id' => 'Категорія',
            'date_of_creation' => 'Date Of Creation',
        ];
    }

    public static function getNameDocument($id)
    {
        $result = self::find()
            ->asArray()
            ->select(['name_file'])
            ->where(['document_id' => $id])
            ->one();

        $result = array_shift($result);
        return $result;
    }


}
