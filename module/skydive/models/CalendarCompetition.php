<?php

namespace app\module\skydive\models;

use Yii;

/**
 * This is the model class for table "calendar_competition".
 *
 * @property integer $id
 * @property string $name
 * @property string $file
 * @property string $date
 */
class CalendarCompetition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calendar_competition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'file'], 'required'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 45],
            [['file'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'file' => 'File',
            'date' => 'Date',
        ];
    }

    public static function getNameDocument($id)
    {
        
    }
}
