<?php

namespace app\module\skydive\models;

use Yii;

/**
 * This is the model class for table "carousel".
 *
 * @property integer $id
 * @property integer $title
 * @property string $file_name
 * @property integer $active
 */
class Carousel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carousel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'active'], 'required'],
            [['active'], 'integer'],
            [['title'], 'string', 'max' => 125],
            [['file_name'], 'file', 'extensions' => 'gif, jpg, jpeg', 'maxSize' => 250000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'file_name' => 'Картинка',
            'active' => 'Статус зображення',
        ];
    }

    public static function getImageName($id)
    {
        $result = self::find()
            ->asArray()
            ->select(['file_name'])
            ->where(['id' => $id])
            ->one();

        $result = array_shift($result);
        return $result;
    }
}
