<?php

namespace app\module\skydive\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\skydive\models\News;

/**
 * NewsSearch represents the model behind the search form about `app\module\skydive\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'deskription', 'full_deskription', 'image', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'deskription', $this->deskription])
            ->andFilterWhere(['like', 'full_deskription', $this->full_deskription])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image', $this->date])
            ->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
}
