<?php

namespace app\module\skydive\models;

use Yii;

/**
 * This is the model class for table "category_documents".
 *
 * @property integer $category_id
 * @property string $name
 */
class CategoryDocuments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 35]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'name' => 'Name',
        ];
    }

    public static function getCategoryDocuments()
    {
        $documents_list = self::find()
            ->asArray()
            ->select(['category_id', 'name'])
            ->all();

        $list = [];
        foreach($documents_list as $key)
        {
            $list += [$key['category_id'] => $key['name']];
        }

        return $list;
    }
}
