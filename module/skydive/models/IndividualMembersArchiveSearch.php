<?php

namespace app\module\skydive\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\skydive\models\IndividualMembersArchive;

/**
<<<<<<< HEAD
 * SearchIndividualMembers represents the model behind the search form about `app\module\skydive\models\IndividualMembers`.
=======
 * IndividualMembersArchiveSearch represents the model behind the search form about `app\module\skydive\models\IndividualMembersArchive`.
>>>>>>> homeDevelopers
 */
class IndividualMembersArchiveSearch extends IndividualMembersArchive
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['id'], 'integer'],
            [['full_name', 'category', 'status'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IndividualMembersArchive::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,

        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'status', $this->status]);


        return $dataProvider;
    }
}
