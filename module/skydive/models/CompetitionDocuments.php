<?php

namespace app\module\skydive\models;

use Yii;

/**
 * This is the model class for table "competition_documents".
 *
 * @property integer $document_id
 * @property string $name
 * @property string $description
 * @property string $name_fille
 * @property integer $competition_id
 * @property string $date_of_create
 */
class CompetitionDocuments extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'competition_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['competition_id'], 'integer'],
            [['date_of_create'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name_fille'], 'string', 'max' => 150],
            [['file'], 'file', 'extensions' => 'pdf'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'name' => 'Імя документа',
            'description' => 'Повний опис',
            'name_fille' => 'Імя файлу',
            'file' => 'Виберіть файл',
            'competition_id' => 'Ід змагання',
            'date_of_create' => 'Date Of Create',
        ];
    }

    public static function getAllDeleteNameFile($competition_id)
    {
        return self::deleteAll(['competition_id' => $competition_id]);
    }

    public static function getAllNameFile($competition_id)
    {
        return self::find()
            ->asArray()
            ->select(['name_fille'])
            ->where(['competition_id' => $competition_id])
            ->all();
    }

    public static function getNameDocument($id)
    {
        $result = self::find()
            ->asArray()
            ->select(['name_fille'])
            ->where(['document_id' => $id])
            ->one();

        $result = array_shift($result);
        return $result;
    }

    public static function getAllNameDocuments($id)
    {
        return self::find()
            ->select('name')
            ->where(['documents_id' => $id])
            ->one();
    }
}
