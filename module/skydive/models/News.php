<?php

namespace app\module\skydive\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $deskription
 * @property string $full_deskription
 * @property string $image
 * @property string $date
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'deskription', 'full_deskription'], 'required'],
            [['deskription', 'full_deskription'], 'string'],
            [['date'], 'safe'],
            [['title'], 'string', 'max' => 50],
            [['image'],  'file', 'extensions' => 'gif, jpg, jpeg', 'maxSize' => 35000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'deskription' => 'Короткий опис',
            'full_deskription' => 'Повний опис',
            'image' => 'Картинка',
            'date' => 'Дата',
        ];
    }

    public static function getImageName($id)
    {
        $result = self::find()
            ->asArray()
            ->select(['image'])
            ->where(['id' => $id])
            ->one();

        $result = array_shift($result);
        return $result;
    }
}
