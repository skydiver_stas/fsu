<?php

namespace app\module\skydive\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\skydive\models\Carousel;

/**
 * CarouselSearch represents the model behind the search form about `app\module\skydive\models\Carousel`.
 */
class CarouselSearch extends Carousel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'active'], 'integer'],
            [['title'], 'safe'],
            [['file_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Carousel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'title' => $this->title,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name])
            ->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
}
