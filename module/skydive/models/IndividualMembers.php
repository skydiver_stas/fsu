<?php

namespace app\module\skydive\models;

use Yii;

/**
 * This is the model class for table "individual_members".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $category
 * @property integer $status
 */
class IndividualMembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'individual_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'category', 'status'], 'required'],
            [['status'], 'string'],
            [['full_name'], 'string', 'max' => 255],
            [['category'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'ПІБ',
            'category' => 'Категорія',
            'status' => 'Статус',
        ];
    }

    public static function getAllMembers()
    {
        return self::find()
            ->asArray()
            ->select(['*'])
            ->all();
    }

    public static function updateStatus()
    {
        self::updateAll(['status' => 'Не активний'], 'status = Активный');

    }
}
