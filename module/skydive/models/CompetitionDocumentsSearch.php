<?php

namespace app\module\skydive\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\skydive\models\CompetitionDocuments;

/**
 * CompetitionDocumentsSearch represents the model behind the search form about `app\module\skydive\models\CompetitionDocuments`.
 */
class CompetitionDocumentsSearch extends CompetitionDocuments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_id', 'competition_id'], 'integer'],
            [['name', 'description', 'name_fille', 'date_of_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $competition_id)
    {
        $query = CompetitionDocuments::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'document_id' => $this->document_id,
            'competition_id' => $this->competition_id,
            'date_of_create' => $this->date_of_create,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'name_fille', $this->name_fille])
            ->where(['competition_id' => $competition_id]);

        return $dataProvider;
    }
}
