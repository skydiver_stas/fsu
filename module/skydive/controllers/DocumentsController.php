<?php

namespace app\module\skydive\controllers;

use Yii;
use app\module\skydive\models\Documents;
use app\module\skydive\models\DocumentsSearch;
use app\module\skydive\Access;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\UploadForm;


/**
 * DocumentsController implements the CRUD actions for Documents model.
 */
class DocumentsController extends Access
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Documents models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Documents model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Documents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Documents();

        $model->name_file = '';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($file = UploadedFile::getInstance($model, 'file')) {
                $fileName = uniqid();
                $file = UploadedFile::getInstance($model, 'file');
                $file->saveAs(PATH_DOCUMENT.$fileName.'.'.$file->extension);

                $model->name_file = $fileName.'.'.$file->extension;
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->document_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Documents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $w = (int) $id;
        $model = $this->findModel($id);
        $nameDocumentFromDB = Documents::getNameDocument($id);

        $model->name_file = '';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($file = UploadedFile::getInstance($model, 'file')) {

                if(!empty($nameDocumentFromDB)){
                    unlink(PATH_DOCUMENT.nameDocumentFromDB);
                }

                $fileName = uniqid();
                $file = UploadedFile::getInstance($model, 'file');
                $file->saveAs(PATH_DOCUMENT.$fileName.'.'.$file->extension);

                $model->name_file = $fileName.'.'.$file->extension;
                $model->save();
            } else {
                $model->name_file = $nameDocumentFromDB;
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->document_id]);
        } else {
            return $this->render('update', [
                'model' => $model,

            ]);
        }
    }

    /**
     * Deletes an existing Documents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $nameDocumentFromDB = Documents::getNameDocument($id);
        if(!empty($nameDocumentFromDB)){
            unlink(PATH_DOCUMENT.$nameDocumentFromDB);
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Documents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
