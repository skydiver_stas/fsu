<?php

namespace app\module\skydive\controllers;

use Yii;
use yii\web\Controller;
use app\module\skydive\models\LoginForm;
use app\module\skydive\models;

class DefaultController extends Controller
{

    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest)
        {
            return $this->redirect(Yii::$app->urlManager->createUrl(['skydive/main']));
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(Yii::$app->urlManager->createUrl(['skydive/main']));
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
}
