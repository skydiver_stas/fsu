<?php

namespace app\module\skydive\controllers;


use Yii;
use app\module\skydive\models\News;
use app\module\skydive\models\NewsSearch;
use app\module\skydive\Access;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Access
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $model->image = '';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(UploadedFile::getInstance($model, 'image'))
            {
                $nameImage = uniqid();
                $image = UploadedFile::getInstance($model, 'image');
                $image->saveAs(PATH_IMAGES.$nameImage.'.'.$image->extension);

                $model->image = $nameImage.'.'.$image->extension;
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nameImageFromDB = News::getImageName($id);
        $model->image = '';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(UploadedFile::getInstance($model, 'image')){

                if(!empty($nameImageFromDB)){
                    if(file_exists(PATH_IMAGES.$nameImageFromDB))
                    unlink(PATH_IMAGES.$nameImageFromDB);
                }

                $nameImage = uniqid();
                $image = UploadedFile::getInstance($model, 'image');
                $image->saveAs(PATH_IMAGES.$nameImage.'.'.$image->extension);

                $model->image = $nameImage.'.'.$image->extension;
                $model->save();
            } else {
                $model->image = $nameImageFromDB;
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $nameImageFromDB = News::getImageName($id);
        if(!empty($nameImageFromDB)){
            unlink(PATH_IMAGES.$nameImageFromDB);
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
