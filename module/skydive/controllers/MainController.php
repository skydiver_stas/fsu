<?php

namespace app\module\skydive\controllers;

use Yii;
use yii\web\Controller;

class MainController extends Controller
{
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        {
            return $this->redirect(Yii::$app->urlManager->createUrl(['skydive/default']));
        }

        return $this->render('index');
    }

}
