<?php

namespace app\module\skydive\controllers;

use Yii;
use app\module\skydive\models\Competition;
use app\module\skydive\models\CompetitionSearch;
use app\module\skydive\Access;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;

/**
 * CompetitionController implements the CRUD actions for Competition model.
 */
class CompetitionController extends Access
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Competition models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompetitionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Competition model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Competition model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Competition();

        /**
         * saving document pdf in folder and name in DB
         */

        $model->regulation = '';
        $model->provisions = '';
        $model->protocols = '';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(UploadedFile::getInstance($model, 'regulation')){

                $regulationName = uniqid();
                $regulation = UploadedFile::getInstance($model, 'regulation');
                $regulation->saveAs(PATH_DOCUMENT.$regulationName.'.'.$regulation->extension);

                $model->regulation = $regulationName.'.'.$regulation->extension;

            }

            if(UploadedFile::getInstance($model, 'provisions')){

                $provisionsName = uniqid();
                $provisions = UploadedFile::getInstance($model, 'provisions');
                $provisions->saveAs(PATH_DOCUMENT.$provisionsName.'.'.$provisions->extension);

                $model->provisions = $provisionsName.'.'.$provisions->extension;

            }

            if(UploadedFile::getInstance($model, 'protocols')){

                $protocolsName = uniqid();
                $protocols = UploadedFile::getInstance($model, 'protocols');
                $protocols->saveAs(PATH_DOCUMENT.$protocolsName.'.'.$protocols->extension);

                $model->protocols = $protocolsName.'.'.$protocols->extension;

            }

            if(UploadedFile::getInstance($model, 'regulation') || UploadedFile::getInstance($model, 'regulation') || UploadedFile::getInstance($model, 'protocols')){
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Competition model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nameDocument = Competition::getNameDocument($id);

        $nameRegulation = $nameDocument['regulation'];
        $nameProvisions = $nameDocument['provisions'];
        $nameProtocols = $nameDocument['protocols'];

        $model->regulation = '';
        $model->provisions = '';
        $model->protocols = '';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(UploadedFile::getInstance($model, 'regulation')){

                if(!empty($nameRegulation)){
                    if(file_exists(PATH_DOCUMENT.$nameRegulation))
                    unlink(PATH_DOCUMENT.$nameRegulation);
                }

            $regulationName = uniqid();
            $regulation = UploadedFile::getInstance($model, 'regulation');
            $regulation->saveAs(PATH_DOCUMENT.$regulationName.'.'.$regulation->extension);

            $model->regulation = $regulationName.'.'.$regulation->extension;


            } else {
                    $model->regulation = $nameRegulation;
                }


        if (UploadedFile::getInstance($model, 'provisions')) {

            if(!empty($nameProvisions)){
                if(file_exists(PATH_DOCUMENT.$nameProvisions))
                unlink(PATH_DOCUMENT.$nameProvisions);
            }
                $provisionsName = uniqid();
                $provisions = UploadedFile::getInstance($model, 'provisions');
                $provisions->saveAs(PATH_DOCUMENT.$provisionsName.'.'.$provisions->extension);

                $model->regulation = $provisionsName.'.'.$provisions->extension;
        } else {
            $model->provisions = $nameProvisions;
        }

        if(UploadedFile::getInstance($model, 'protocols')){

            if (!empty($nameProtocols)){
                if(file_exists(PATH_DOCUMENT.$nameProtocols))
                unlink(PATH_DOCUMENT.$nameProtocols);
            }

                $protocolsName = uniqid();
                $protocols = UploadedFile::getInstance($model, 'protocols');
                $protocols->saveAs(PATH_DOCUMENT.$protocolsName.'.'.$protocols->extension);

                $model->protocols = $protocolsName.'.'.$protocols->extension;
            } else {
                $model->protocols = $nameProtocols;
            }

         $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'nameRegulation' => $nameRegulation,
                'nameProvisions' => $nameProvisions,
                'nameProtocols' => $nameProtocols,
            ]);
        }
    }

    /**
     * Deletes an existing Competition model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $nameDocument = Competition::getNameDocument($id);

        $nameRegulation = $nameDocument['regulation'];
        $nameProvisions = $nameDocument['provisions'];
        $nameProtocols = $nameDocument['protocols'];

        if(!empty($nameRegulation)){
            if(file_exists(PATH_DOCUMENT.$nameRegulation))
            unlink(PATH_DOCUMENT.$nameRegulation);
        }

        if(!empty($nameProvisions)){
            if(file_exists(PATH_DOCUMENT.$nameRegulation))
            unlink(PATH_DOCUMENT.$nameProvisions);
        }

        if (!empty($nameProtocols)){
            if(file_exists(PATH_DOCUMENT.$nameRegulation))
            unlink(PATH_DOCUMENT.$nameProtocols);
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Competition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Competition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Competition::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
