<?php

namespace app\module\skydive\controllers;

use Yii;
use app\module\skydive\models\IndividualMembers;
use app\module\skydive\models\IndividualMembersSearch;
use app\module\skydive\models\IndividualMembersArchive;
use app\module\skydive\models\IndividualMembersArchiveSearch;
use app\module\skydive\Access;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\db\ActiveRecord;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * IndividualMembersController implements the CRUD actions for IndividualMembers model.
 */
class IndividualMembersController extends Access
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IndividualMembers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndividualMembersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $yearLastUpdate = Yii::$app->db->createCommand('SELECT year FROM individual_members_date_update')->queryOne();

        $yearLastUpdate = array_shift($yearLastUpdate);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'yearLastUpdate' => $yearLastUpdate
        ]);
    }

    /**
     * Displays a single IndividualMembers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IndividualMembers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IndividualMembers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing IndividualMembers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing IndividualMembers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /*
     * Отримання списку архіву членів з таблички 'individual_members_archive'
     * Get archive list members where the table of 'individual_members_archive'
    */


    public function actionAllUpdate()
    {
        $allMemebers = IndividualMembers::getAllMembers();
        $message = '';

        if(!empty($allMemebers))
        {
            IndividualMembersArchive::dropTable();
            IndividualMembersArchive::insertMembers($allMemebers);
            IndividualMembers::updateStatus();
            Yii::$app->db->createCommand()->update('individual_members_date_update', ['year' => YEAR_NOW])->execute();
            $message = 'Виконано успішно';
        }

        else
        {
            $message = 'Не виконано';
        }

        return $this->redirect(['index']);
    }

    private static function createFile($fileName)
    {
        $allMemebers = IndividualMembers::getAllMembers();

        if(!empty($allMemebers))
        {
            if(file_exists(PATH_TEMPORARRY_FILE.$fileName.".csv"))
                unlink(PATH_TEMPORARRY_FILE.$fileName.".csv");
            $file = fopen(PATH_TEMPORARRY_FILE.$fileName.".csv", "w");

            foreach($allMemebers as $key)
            {
                fputcsv($file, $key);
            }

            fclose($file);
        }
    }

    public function actionCreateFileArchive()
    {
        $fileName = 'archive';
        self::createFile($fileName);
        return $this->redirect(['archive']);
    }

    public function actionCreateFileActual()
    {
        $fileName = 'actual';
        self::createFile($fileName);
        return $this->redirect(['index']);
    }



    /**
     * Finds the IndividualMembers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IndividualMembers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IndividualMembers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
