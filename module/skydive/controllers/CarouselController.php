<?php

namespace app\module\skydive\controllers;

use Yii;
use app\module\skydive\models\Carousel;
use app\module\skydive\models\CarouselSearch;
use app\module\skydive\Access;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * CarouselController implements the CRUD actions for Carousel model.
 */
class CarouselController extends Access
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Carousel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarouselSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Carousel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Carousel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Carousel();

        $model->file_name = '';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(UploadedFile::getInstance($model, 'file_name'))
            {
                $imageName= uniqid();
                $image = UploadedFile::getInstance($model, 'file_name');
                $image->saveAs(PATH_CAROUSEL.$imageName.'.'.$image->extension);

                $model->file_name = $imageName.'.'.$image->extension;
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Carousel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $imageNameFromDB = Carousel::getImageName($id);
        $model->file_name = '';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(UploadedFile::getInstance($model, 'file_name'))
            {
                if(!empty($imageNameFromDB)){
                    if(file_exists(PATH_CAROUSEL.$imageNameFromDB))
                    unlink(PATH_CAROUSEL.$imageNameFromDB);
                }

                $imageName= uniqid();
                $image = UploadedFile::getInstance($model, 'file_name');
                $image->saveAs(PATH_CAROUSEL.$imageName.'.'.$image->extension);

                $model->file_name = $imageName.'.'.$image->extension;
                $model->save();
            } else {
                $model->file_name = $imageNameFromDB;
                $model->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Carousel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $imageNameFromDB = Carousel::getImageName($id);
        if(!empty($imageNameFromDB)){
            if(file_exists(PATH_CAROUSEL.$imageNameFromDB))
            unlink(PATH_CAROUSEL.$imageNameFromDB);
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Carousel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Carousel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Carousel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
