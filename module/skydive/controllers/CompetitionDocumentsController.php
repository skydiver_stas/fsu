<?php

namespace app\module\skydive\controllers;

use Yii;
use app\module\skydive\models\CompetitionDocuments;
use app\module\skydive\models\CompetitionDocumentsSearch;
use app\module\skydive\models\Competition;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;

/**
 * CompetitionDocumentsController implements the CRUD actions for CompetitionDocuments model.
 */
class CompetitionDocumentsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompetitionDocuments models.
     * @return mixed
     */
    public function actionIndex($competition_id)
    {
        $searchModel = new CompetitionDocumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $competition_id);
        $nameCompetition = Competition::getNameCompetition($competition_id);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'competition_id' => $competition_id,
            'nameCompetition' => $nameCompetition,
        ]);
    }

    /**
     * Displays a single CompetitionDocuments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $competition_id)
    {
        $nameCompetition = Competition::getNameCompetition($id);

        return $this->render('view', [
            'competition_id' => $competition_id,
            'model' => $this->findModel($id),
            'nameCompetition' =>$nameCompetition,
        ]);
    }

    /**
     * Creates a new CompetitionDocuments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($competition_id)
    {
        $model = new CompetitionDocuments();

        $model->name_fille = '';
        $model->competition_id = '';
        $model->competition_id = $competition_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {



            if(UploadedFile::getInstance($model, 'file')){

                $nameFile = uniqid();
                $file = UploadedFile::getInstance($model, 'file');
                $file->saveAs(PATH_DOCUMENT_FOR_COMPETITION.$nameFile.'.'.$file->extension);

                $model->name_fille = $nameFile.'.'.$file->extension;


            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->document_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CompetitionDocuments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id , $competition_id)
    {
        $model = $this->findModel($id);
        $nameCompetition = Competition::getNameCompetition($id);
        $nameDocumentFromDB = CompetitionDocuments::getNameDocument($id);

        $model->name_fille = '';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($file = UploadedFile::getInstance($model, 'file')) {
                if(!empty($nameDocumentFromDB)){
                    unlink(PATH_DOCUMENT_FOR_COMPETITION.$nameDocumentFromDB);
                }

                $nameFile = uniqid();
                $file = UploadedFile::getInstance($model, 'file');
                $file->saveAs(PATH_DOCUMENT_FOR_COMPETITION.$nameFile.'.'.$file->extension);

                $model->name_fille = $nameFile.'.'.$file->extension;
                $model->save();
            } else {
                $model->name_fille = $nameDocumentFromDB;
                $model->save();
            }

            return $this->redirect(['view',
                'id' => $model->document_id,
                'nameCompetition' => $nameCompetition,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'competition_id' => $competition_id,
                'nameCompetition' => $nameCompetition,
            ]);
        }
    }

    /**
     * Deletes an existing CompetitionDocuments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $nameDocumentFromDB = CompetitionDocuments::getNameDocument($id);
        if(!empty($nameDocumentFromDB)){
            unlink(PATH_DOCUMENT_FOR_COMPETITION.$nameDocumentFromDB);
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompetitionDocuments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompetitionDocuments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompetitionDocuments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
