<?php

namespace app\module\skydive\controllers;

use app\module\skydive\Access;
use app\module\skydive\models\Competition;
use yii\data\Pagination;

class ListCompetitionController extends Access
{
    public function actionIndex()
    {
        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => Competition::getCountCompetition(),
        ]);

        $listCompetition = Competition::getAllCompetition($pagination->offset, $pagination->limit);

        return $this->render('index',
            [
                'pagination' => $pagination,
                'listCompetition' => $listCompetition,
            ]
        );
    }

}
