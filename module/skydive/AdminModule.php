<?php

namespace app\module\skydive;

use Yii;

class AdminModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\module\skydive\controllers';

    public function init()
    {

        parent::init();
        // set the layout path
        $this->setLayoutPath($this->getViewPath() . '/layouts');
        // set the layout
        $this->layout = 'skydive';

        // custom initialization code goes here
    }
}
