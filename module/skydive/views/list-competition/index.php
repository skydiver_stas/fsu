<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;
$this->title = 'Список змагань';
$this->params['breadcrumbs'][] = $this->title;

?>
    <h1><?= Html::encode($this->title) ?></h1>

<?php if(!empty($listCompetition)): ?>
    <?php foreach($listCompetition as $key): ?>
        <div class="news">
            <div class="text-news">
                <h3><?= Html::encode($key['name']); ?></h3>
                <p><?= nl2br($key['description']); ?></p>
                <p><a href="<?= Yii::$app->urlManager->createUrl(['skydive/competition-documents', 'competition_id' => $key['competition_id']]); ?>">Перегляд документів</a></p>
            </div>

        </div>
        <div class="news-date">
            <p>Дата публікації - <?= $key['date_of_create']; ?></p>
        </div>
    <?php endforeach; ?>
    <?= LinkPager::widget(['pagination' => $pagination]) ?>
<?php endif; ?>