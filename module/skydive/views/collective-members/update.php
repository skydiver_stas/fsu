<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\CollectiveMembers */

$this->title = 'Оновити колективного члена: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Колективний член', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Оновити';
?>
<div class="collective-members-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
