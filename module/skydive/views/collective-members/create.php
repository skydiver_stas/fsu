<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\CollectiveMembers */

$this->title = 'Створення колективного члена';
$this->params['breadcrumbs'][] = ['label' => 'Колективний член', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collective-members-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
