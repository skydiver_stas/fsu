<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\CollectiveMembers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Колективний член', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collective-members-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Оновити', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'region',
            'contacts:ntext',
        ],
    ]) ?>

</div>
