<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\Documents */

$this->title = 'Створити документ';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
