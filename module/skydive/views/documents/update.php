<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\Documents */

$this->title = 'Оновлення документу: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Документи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->document_id]];
$this->params['breadcrumbs'][] = 'Оновлення';
?>
<div class="documents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
