<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\module\skydive\models\CategoryDocuments;
use yii\widgets\ActiveField;
use yii\validators\Validator;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\Documents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="documents-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]);

    $params = ['prompt' => 'Виберіть категорію...'];

    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

   <!-- <?= $form->field($model, 'name_file')->textInput(['maxlength' => true]) ?> -->

    <?=$form->field($model, 'file')->fileInput() ?>

    <?= $form->field($model, 'category_id')->dropDownList(CategoryDocuments::getCategoryDocuments(), $params) ?>

   <!-- <?= $form->field($model, 'date_of_creation')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Оновити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
