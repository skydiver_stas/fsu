<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\IndividualMembers */

$this->title = 'Змінити: ' . ' ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Індивідуальні члени', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="individual-members-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
