<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\module\skydive\models\SearchIndividualMembers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Індивідуальні члени';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="individual-members-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Додати індивідуального члена', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Перегляд архіву', ['individual-members-archive/'], ['class' => 'btn btn-success']) ?>

        <?php
        if(file_exists(PATH_TEMPORARRY_FILE."actual.csv"))
        {
            $fileName = PATH_TEMPORARRY_FILE."actual.csv";
            echo "<a href='$fileName' class='btn btn-success' download>Завантажити файл</a>";
            $nameButton = 'Оновити ';
        }
        else
        {
            $nameButton = 'Створити ';
        }
        ?>
        <?= Html::a( $nameButton.'"csv" файл', ['create-file-actual'], ['class' => 'btn btn-success']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'full_name',
            'category',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p>
        <?php

            if($yearLastUpdate != YEAR_NOW)
            {
                Modal::begin([
                    'header' => '<h2>Ви впевнені що хочете оновити архів та статуси!</h2>
                    <p>Це можливо зробити один раз в рік і зазвичай це робиться на початку року</p>',
                    'toggleButton' => [
                        'tag' => 'button',
                        'class' => 'btn btn-success',
                        'label' => 'Оновити статуси та архів',
                    ]
                ]);

                echo Html::a('Оновити статуси та архів', ['all-update'], ['class' => 'btn btn-success']);

                Modal::end();
            }
        ?>
    </p>

</div>
