<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\IndividualMembers */

$this->title = 'Додати нового індивідуального члена фпсу';
$this->params['breadcrumbs'][] = ['label' => 'Індивідуальні члени', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="individual-members-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
