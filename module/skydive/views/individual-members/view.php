<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\IndividualMembers */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Індивідуальні члени', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="individual-members-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Змінити', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви впевнині чо хочете видалити?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'full_name',
            'category',
            'status',
        ],
    ]) ?>

</div>
