<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\CompetitionDocuments */

$this->title = 'Створити документ для змагання';
$this->params['breadcrumbs'][] = ['label' => 'Документ для змагання', 'url' => ['/skydive/list-competition']];
$this->params['breadcrumbs'][] = ['label' => 'Документ для змагання', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="competition-documents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
