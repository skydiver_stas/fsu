<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\module\skydive\models\Competition;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\CompetitionDocuments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="competition-documents-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]);
        $params = ['prompt' => 'Виберіть категорію...'];
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?=$form->field($model, 'file')->fileInput() ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Оновити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
