<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\CompetitionDocuments */
$url = 'index?competition_id='.$competition_id;
$this->title = 'Оновити документ для змагання: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Документ для змагання', 'url' => ['/skydive/list-competition']];
$this->params['breadcrumbs'][] = ['label' => $nameCompetition, 'url' => [$url]];
$this->params['breadcrumbs'][] = 'Оновити "'. $model->name. '"';
?>
<div class="competition-documents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
