<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\module\skydive\models\CompetitionDocumentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $nameCompetition;
$this->params['breadcrumbs'][] = ['label' => 'Документ для змагання', 'url' => ['/skydive/list-competition']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="competition-documents-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Створення документу для змагання', ['/skydive/competition-documents/create/', 'competition_id' => $competition_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'document_id',
            'name',
            'description:ntext',
            'name_fille',
            'competition_id',
            // 'date_of_create',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) use ($competition_id) {
                            $url .= '&competition_id='.$competition_id; //This is where I want to append the $lastAddress variable.
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                        },
                    'view' => function ($url, $model) use ($competition_id){
                            $url .= '&competition_id='.$competition_id; //This is where I want to append the $lastAddress variable.
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                        },
                ],
            ],
        ],

    ]); ?>

</div>
