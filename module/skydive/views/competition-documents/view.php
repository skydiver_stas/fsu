<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\CompetitionDocuments */
$url = 'index?competition_id='.$competition_id;
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Документ для змагання', 'url' => ['/skydive/list-competition']];
$this->params['breadcrumbs'][] = ['label' => $nameCompetition, 'url' => [$url]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="competition-documents-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Оновити', ['update', 'id' => $model->document_id, 'competition_id' => $competition_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('видалити', ['delete', 'id' => $model->document_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'document_id',
            'name',
            'description:ntext',
            'name_fille',
            'competition_id',
            'date_of_create',
        ],
    ]) ?>

</div>
