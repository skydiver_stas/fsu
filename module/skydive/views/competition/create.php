<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\Competition */

$this->title = 'Створити змагання';
$this->params['breadcrumbs'][] = ['label' => 'Змагання', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="competition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
