<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\Competition */

$this->title = 'Змінити змагання: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Змагання', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->competition_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="competition-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
