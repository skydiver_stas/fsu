<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\Carousel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]);
        $items = [
            '0' => 'Не активна',
            '1' => 'Активна'
        ];

        $params = [
          'prompt' => 'Виберіть статус'
        ];
    ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'file_name')->fileInput() ?>

    <?= $form->field($model, 'active')->dropDownList($items, $params) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Оновити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
