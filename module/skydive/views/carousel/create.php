<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\Carousel */

$this->title = 'Додати картинку в карусель';
$this->params['breadcrumbs'][] = ['label' => 'Карусель', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
