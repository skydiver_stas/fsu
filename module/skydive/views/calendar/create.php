<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\CalendarCompetition */

$this->title = 'Create Calendar Competition';
$this->params['breadcrumbs'][] = ['label' => 'Calendar Competitions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calendar-competition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
