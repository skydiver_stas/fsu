<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\IndividualMembersArchive */

$this->title = 'Створити індивідуального члена';
$this->params['breadcrumbs'][] = ['label' => 'Індивідуальні члени', 'url' => ['individual-members/']];
$this->params['breadcrumbs'][] = ['label' => 'Індивідуальні члени архів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="individual-members-archive-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
