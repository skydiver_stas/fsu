<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\IndividualMembersArchive */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="individual-members-archive-form">

    <?php $form = ActiveForm::begin();

    $items = [
        'Активний' => 'Активний',
        'Не активний' => 'Не активний'
    ];
    $params = ['prompt' => 'Виберіть статус...'];

    ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($items, $params) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Оновити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
