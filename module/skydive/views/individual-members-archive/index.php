<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\module\skydive\models\IndividualMembersArchiveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Individual Members Archives';
$this->params['breadcrumbs'][] = ['label' => 'Індивідуальні члени', 'url' => ['individual-members/']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="individual-members-archive-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Додати індивідуального члена', ['create'], ['class' => 'btn btn-success']) ?>

        <?php
        if(file_exists(PATH_TEMPORARRY_FILE."archive_file.csv"))
        {
            $fileName = PATH_TEMPORARRY_FILE."archive_file.csv";
            echo "<a href='$fileName' class='btn btn-success' download>Завантажити файл</a>";
            $nameButton = 'Оновити ';
        }
        else
        {
            $nameButton = 'Створити ';
        }
        ?>
        <?= Html::a( $nameButton.'"csv" файл', ['create-file-archive'], ['class' => 'btn btn-success']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'full_name',
            'category',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
