<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\skydive\models\IndividualMembersArchive */

$this->title = 'Оновити: ' . ' ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Індивідуальні члени', 'url' => ['individual-members/']];
$this->params['breadcrumbs'][] = ['label' => 'Індивідуальні члени архів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'оновлення';
?>
<div class="individual-members-archive-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
