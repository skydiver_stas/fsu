<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 24.03.16
 * Time: 11:46
 */
define('PATH_DOCUMENT', 'documents_pdf/');
define('PATH_DOCUMENT_FOR_COMPETITION', 'documents_for_competition/');
define('PATH_IMAGES', 'image_content/');
define('PATH_CAROUSEL', 'carousel_image/');
define('PATH_TEMPORARRY_FILE', 'temporary_file/');

define('PATH_DOCUMENT_SHOW', '/fsu/web/documents_pdf/');
define('PATH_DOCUMENT_FOR_COMPETITION_SHOW', '/fsu/web/documents_for_competition/');
define('PATH_IMAGES_SHOW', '/fsu/web/image_content/');
define('PATH_CAROUSEL_SHOW', '/fsu/web/carousel_image/');
define('PATH_IMAGE_SHOW', '/fsu/web/image/');

define('DATE_CREATE_SITE', 2016);
define('YEAR_NOW', date("Y"));